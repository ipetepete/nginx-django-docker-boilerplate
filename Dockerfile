FROM python:3.6.6-jessie

RUN apt update
RUN apt install -y \
   supervisor \
   libc-dev \
   build-essential \
   git \
   rsync

RUN apt install -y nginx 


# ---- Setup all the configfiles
COPY ./config/nginx.conf /etc/nginx/nginx.conf
COPY ./config/supervisord.conf /etc/supervisord.conf
COPY ./config/nginx-app.conf /etc/nginx/sites-available/default
COPY ./config/supervisor-app.conf /etc/supervisor/conf.d/
COPY ./config/uwsgi.ini /config/
COPY ./config/uwsgi_params /config/
COPY ./container_scripts /scripts

WORKDIR /srv/app

RUN pip3 install uwsgi

# ---- Copy over local files
# COPY ./src/requirements.txt /srv/app
# RUN pip3 install -r requirements.txt 

EXPOSE 80 8000
CMD ['supervisord', '-n', '-c', '/etc/supervisor/conf.d/supervisor-app.conf']
