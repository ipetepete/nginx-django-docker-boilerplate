# Boilerplate for django + nginx

## Things to note
 - assumes there will be an environment variable `$APP_NAME` with the name of the app (set it in docker-compose.yml)
 - place app in container location `/srv/app`
 - Supervisor admin login (change in docker-compose.yml):
    - Username: admin
    - Password: SuP3rVizM3
